import Vue from 'vue'
import Router from 'vue-router'

import Crawl from '@/components/Crawl'
import Questions from '@/components/Questions'
import Predict from '@/components/Predict'
import Visualize from '@/components/Visualize'
import Export from '@/components/Export'

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/crawl',
            name: 'Crawl',
            component: Crawl
        },
        {
            path: '/questions',
            name: 'Questions',
            component: Questions
        },
        {
            path: '/predict',
            name: 'Predict',
            component: Predict
        },
        {
            path: '/visualize',
            name: 'Visualize',
            component: Visualize
        },
        {
            path: '/export',
            name: 'Export',
            component: Export
        }
    ]
})
