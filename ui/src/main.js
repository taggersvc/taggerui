import Vue from 'vue'

import axios from 'axios'
import VueAxios from 'vue-axios'

import router from './router'
import App from './App.vue'

import './../node_modules/bulma/css/bulma.css';

Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router
}).$mount('#app');
