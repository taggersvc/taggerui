# Tagger UI Application

```
$ cd ui
$ sudo apt install npm -y
$ npm install
$ npm run serve
```

## Xpaths inputs
```
- question_xpath: h3/a
- tags_xpath: div[2]/a
- url_xpath: h3/a
```